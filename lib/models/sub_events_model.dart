class SubEventsModel{

  String mainEventID, subEventID, subEventName, subEventCategory, subEventFee, subEventsPersons, subEventDescription;
  bool loading;

  // SubEventsModel({required this.mainEventID, required this.subEventID, required this.subEventName, required this.subEventCategory, required this.subEventFee, required this.subEventsPersons, required this.subEventDescription});
  SubEventsModel({this.mainEventID = "", this.subEventID = "", this.subEventName = "", this.subEventCategory = "", this.subEventFee = "", this.subEventsPersons = "", this.subEventDescription = "", this.loading=false});

  factory SubEventsModel.fromJson(Map<String, dynamic> json){

    final main_event_ID = json['mainEventID'] ?? "";
    final sub_event_ID = json['subEventID'] ?? "";
    final sub_event_name = json['subEventName'] ?? "";
    final sub_event_category = json['subEventCategory'] ?? "";
    final sub_event_fee = json['subEventFee'] ?? "";
    final sub_event_persons = json['subEventsPersons'] ?? "";
    final sub_event_description = json['subEventDescription'] ?? "";

    return SubEventsModel(mainEventID: main_event_ID, subEventID: sub_event_ID, subEventName: sub_event_name, subEventCategory: sub_event_category, subEventFee: sub_event_fee, subEventsPersons: sub_event_persons, subEventDescription: sub_event_description);

  }

  Map<String, dynamic> toJson() {
    return {
      'mainEventID': mainEventID,
      'subEventID': subEventID,
      'subEventName': subEventName,
      'subEventCategory': subEventCategory,
      'subEventFee': subEventFee,
      'subEventsPersons': subEventsPersons,
      'subEventDescription': subEventDescription,
    };
  }

}