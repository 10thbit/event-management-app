import 'dart:convert';

class EventModel {
  String eventID,
      creatorID,
      eventName,
      eventOrganizer,
      eventDescription,
      eventStartDate,
      eventEndDate;
  List<String> participantsList;

  // EventModel({required this.eventID, required this.eventName, required this.eventOrganizer, required this.eventDescription, required this.eventStartDate, required this.eventEndDate, required this.eventCode});
  EventModel({
    this.eventID = "",
    this.creatorID = "",
    this.eventName = "",
    this.eventOrganizer = "",
    this.eventDescription = "",
    this.eventStartDate = "",
    this.eventEndDate = "",
    this.participantsList = const [],
  });

  factory EventModel.fromJson(dynamic json) {
    return EventModel(
      eventID: json['eventID'] ?? "",
      creatorID: json['creatorID'] ?? "",
      eventName: json['eventName'] ?? "",
      eventOrganizer: json['eventOrganizer'] ?? "",
      eventDescription: json['eventDescription'] ?? "",
      eventStartDate: json['eventStartDate'] ?? "",
      eventEndDate: json['eventEndDate'] ?? "",
      participantsList: json['participants'] != null
          ? List<String>.from(json['participants'])
          : [],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'eventID': eventID,
      'creatorID': creatorID,
      'eventName': eventName,
      'eventDescription': eventDescription,
      'eventOrganizer': eventOrganizer,
      'eventStartDate': eventStartDate,
      'eventEndDate': eventEndDate,
      'participants': participantsList,
    };
  }
}
