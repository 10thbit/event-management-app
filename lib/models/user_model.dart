class UserModel {
  String userEmail, userPassword, userName, userPhone, userID, adminType;
  bool isAdmin, loading, isButtonActive;

  UserModel({this.userEmail="", this.userPassword="", this.userName="", this.userPhone="", this.userID="", this.adminType="", this.isAdmin=false, this.loading=false, this.isButtonActive=true});

  factory UserModel.fromJson(dynamic json) {
    if (json != null) {
      final user_email = json['userEmail'] ?? "";
      final user_password = json['userPassword'] ?? "";
      final user_name = json['userName'] ?? "";
      final user_phone = json['userPhone'] ?? "";
      final user_id = json['userUID'] ?? "";
      final admin_type = json['userAdminType'] ?? "";
      final is_admin = json['isAdmin'] ?? false;

      return UserModel(userName: user_name, userEmail: user_email, userPassword: user_password, userPhone: user_phone, userID: user_id, adminType: admin_type, isAdmin: is_admin);

    }
    return UserModel();
  }

  Map<String, dynamic> toJson() {
    return {
      'userEmail': userEmail,
      'userPassword': userPassword,
      'userName': userName,
      'userPhone': userPhone,
      'userUID': userID,
      'isAdmin': isAdmin,
      if (isAdmin) 'userAdminType': adminType,
    };
  }
}
