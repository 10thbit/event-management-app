
class Routes{

  static const authenticationScreen = '/';

  static const userDashboard = 'UserDashboard';

  static const adminPanelLoginScreen = 'AdminPanelLogin';

  static const adminPanelRegisterScreen = 'AdminPanelRegisterScreen';

  static const adminPanelDashboard = 'AdminDashboard';

  static const addEventPage = 'AddEventPage';

  static const manageEventPage = 'ManageEventsPage';

  static const addSubEventPage = 'AddSubEventPage';

  static const eventViewPage = 'EventViewPage';

  static const subEventViewPage = 'SubEventViewPage';

  static const updateEventPage = 'UpdateEventPage';

  static const updateSubEventPage = 'UpdateSubEventPage';

}