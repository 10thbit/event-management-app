
enum AdminType
{
  individual,
  university,
  organization
}

class AppStrings {
  static const individual = "Individual";
  static const university = "University";
  static const organization = "Organization";
}