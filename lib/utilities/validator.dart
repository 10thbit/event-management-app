import 'package:get/get.dart';



class FieldValidator {
 

  static String? validateEmail(String value) {
    if (value.isEmpty) {
      return "email_is_required";
    }
    if (!RegExp(
            r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)"
            r"*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
        .hasMatch(value.trim())) {
      return "please_enter_a_valid_email_address";
    }
    return null;
  }

  static String? validatePassword(String value) {
    if (value.isEmpty) {
      return "password_is_required";
    }
    if (value.length < 6) {
      return "password_should_consists_of_minimum_six_character";
    }
    if (!RegExp(r"^(?=.*?[0-9])").hasMatch(value)) {
      return "password_should_include_at_least_one_number";
    }
    return null;
  }
  static String? validateConfirmPassword(String value1,String value2) {
    if(value2.isEmpty) return "password_is_required";
    if (value1!=value2){
      return "password_does_not_match";
    }
    return null;
  }

  static String? validateOldPassword(String value) {
    if (value.isEmpty) {
      return "old_password_is_required";
    }
    if (value.length < 6) {
      return "old_password_should_consists_of_minimum_six_character";
    }
    if (!RegExp(r"^(?=.*?[0-9])").hasMatch(value)) {
      return "old_password_should_include_at_least_one_number";
    }
    return null;
  }
  static String? validatePasswordMatch(String value, String pass2) {
    if (value.isEmpty) return "Confirm password is required";
    if (value != pass2) {
      return 'Password didn\'t match';
    }
    return null;
  }

  static String? validateName(String value) {
    if (value.isEmpty) {
      return "name_is_required";
    }
    if (value.length <= 2) {
      return "name_is_too_short";
    }
    if (!RegExp(r"^([ \u00c0-\u01ffa-zA-Z'\-])+$").hasMatch(value)) {
      return "please_enter_a_valid_name";
    }
    return null;
  }


 static String? validateDate(String value) {
    if (value.isEmpty) {
      return "date_is_required";
    }

    int year;
    int month;
    // The value contains a forward slash if the month and year has been
    // entered.
    if (value.contains( RegExp(r'(\/)'))) {
      //print("valid value in regx");
      var split = value.split( RegExp(r'(\/)'));
      // The value before the slash is the month while the value to right of
      // it is the year.
      month = int.parse(split[0]);
      year = int.parse(split[1]);

    } else { 
     // print("invalid value for regx");// Only the month was entered
      month = int.parse(value.substring(0, (value.length)));
      year = -1; // Lets use an invalid year intentionally
    }

    if ((month < 1) || (month > 12)) {
      // A valid month is between 1 (January) and 12 (December)
      return "expiry_month_is_invalid";
      
    }

    var fourDigitsYear = convertYearTo4Digits(year);
    if ((fourDigitsYear < 1) || (fourDigitsYear > 2099)) {
      // We are assuming a valid year should be between 1 and 2099.
      // Note that, it's valid doesn't mean that it has not expired.
      return "expiry_year_is_invalid";
      
    }

    if (!hasDateExpired(month, year)) {
      return "card_has_expired";
      
    }
    return null;
  }


  /// Convert the two-digit year to four-digit year if necessary
  static int convertYearTo4Digits(int year) {
    if (year < 100 && year >= 0) {
      var now = DateTime.now();
      String currentYear = now.year.toString();
      String prefix = currentYear.substring(0, currentYear.length - 2);
      year = int.parse('$prefix${year.toString().padLeft(2, '0')}');
    }
    return year;
  }


  static bool hasDateExpired(int month, int year) {
    return !(month == null || year == null) && isNotExpired(year, month);
  }

  static bool isNotExpired(int year, int month) {
    // It has not expired if both the year and date has not passed
    return !hasYearPassed(year) && !hasMonthPassed(year, month);
  }

 static bool hasMonthPassed(int year, int month) {
    var now = DateTime.now();
    // The month has passed if:
    // 1. The year is in the past. In that case, we just assume that the month
    // has passed
    // 2. Card's month (plus another month) is less than current month.
    return hasYearPassed(year) ||
        convertYearTo4Digits(year) == now.year && (month < now.month + 1);
  }

  static bool hasYearPassed(int year) {
    int fourDigitsYear = convertYearTo4Digits(year);
    var now = DateTime.now();
    // The year has passed if the year we are currently, is greater than card's
    // year
    return fourDigitsYear < now.year;
  }

  static String? validateThisField(String value) {
    if (value.isEmpty) {
      return "this_field_required";
    }
    return null;
  }


}
