import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:event_manager/models/events_model.dart';
import 'package:event_manager/models/sub_events_model.dart';
import 'package:event_manager/widgets/toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:uuid/uuid.dart';

final _firestore = FirebaseFirestore.instance;

class SubEventProvider extends ChangeNotifier{

  EventModel eventModel = EventModel();
  SubEventsModel subEventModel = SubEventsModel();

  void updateSubEventFields({
    String? subEventName,
    String? subEventCategory,
    String? subEventFee,
    String? subEventsPersons,
    String? subEventDescription,
    bool loading = false,
  }) {
    subEventModel = SubEventsModel(
      subEventName: subEventName ?? subEventModel.subEventName,
      subEventCategory: subEventCategory ?? subEventModel.subEventCategory,
      subEventFee: subEventFee ?? subEventModel.subEventFee,
      subEventsPersons: subEventsPersons ?? subEventModel.subEventsPersons,
      subEventDescription: subEventDescription ?? subEventModel.subEventDescription,
      loading: loading,
    );
    notifyListeners();
  }

  // Future<void> addSubEvent(String mainEventID, String subEventID, String subEventName, String subEventCategory, String subEventFee, String subEventsPersons, String subEventDescription) async{
  //   final eventModel = SubEventsModel(mainEventID: mainEventID, subEventID: subEventID, subEventName: subEventName, subEventCategory: subEventCategory, subEventFee: subEventFee, subEventsPersons: subEventsPersons, subEventDescription: subEventDescription);
  //   final jsonMap = eventModel.toJson();
  //
  //   await _firestore.collection('Events').doc(mainEventID).collection('SubEvents').doc(subEventID).set(jsonMap);
  // }

  Future<void> addSubEvent(String mainEventID) async{
    var subEventId = const Uuid().v1();
    subEventModel.subEventID = subEventId;
    subEventModel.mainEventID = mainEventID;
    // final eventModel = SubEventsModel(mainEventID: mainEventID, subEventID: subEventID,);
    // final jsonMap = eventModel.toJson();

    await _firestore.collection('Events').doc(mainEventID).collection('SubEvents').doc(subEventId).set(subEventModel.toJson());
  }

  Future<void> updateSubEvent(Function onUpdate) async {
    await _firestore.collection('Events').doc(subEventModel.mainEventID).collection('SubEvents').doc(subEventModel.subEventID).update(subEventModel.toJson());
    onUpdate();
    notifyListeners();
  }

  Future<void> onRegisterSubEventTapped(String mainEventID, Function onSuccess, Function onError, SubEventsModel subEventModel) async {

    if(subEventModel.subEventName.isNotEmpty && subEventModel.subEventCategory.isNotEmpty && subEventModel.subEventFee.isNotEmpty && subEventModel.subEventsPersons.isNotEmpty && subEventModel.subEventDescription.isNotEmpty){

      updateSubEventFields(loading: true);
      // eventProvider.addSubEvent(eventProvider.eventModel.eventID, Timestamp.now().microsecondsSinceEpoch.toString(), subEventName!, subEventCategory!, subEventFee!, subEventsPersons!, subEventDescription!)

      var subEventId = const Uuid().v1();
      subEventModel.subEventID = subEventId;
      subEventModel.mainEventID = mainEventID;
      // final eventModel = SubEventsModel(mainEventID: mainEventID, subEventID: subEventID,);
      // final jsonMap = eventModel.toJson();

      await _firestore.collection('Events').doc(mainEventID).collection('SubEvents').doc(subEventId).set(subEventModel.toJson())
          .whenComplete(() => onSuccess())
          .onError((error, stackTrace) => () => onError());

    }
    else{
      showToast("Fields Required");
      print("Fields Missing");
    }
    
  }

  Future<void> onUpdateSubEventTapped(Function onSuccess, Function onError, SubEventsModel subEventModel) async {

    if(subEventModel.subEventName.isNotEmpty && subEventModel.subEventCategory.isNotEmpty &&
        subEventModel.subEventFee.isNotEmpty && subEventModel.subEventsPersons.isNotEmpty &&
        subEventModel.subEventDescription.isNotEmpty){

      updateSubEventFields(loading: true);

      print("Main Event ID: ${subEventModel.mainEventID}");
      print("Sub Event ID: ${subEventModel.subEventID}");

      await _firestore.collection('Events').doc(subEventModel.mainEventID).collection('SubEvents').doc(subEventModel.subEventID).update(subEventModel.toJson())
      .whenComplete(() => onSuccess())
      .onError((error, stackTrace) => onError());

      // updateSubEventFields(loading: false);
      // Navigator.pop(context);
      // showToast("Updated Successfully");
      // notifyListeners();

      // updateSubEvent(
      //         (){
      //   updateSubEventFields(loading: false);
      //   Navigator.pop(context);
      //   showToast("Updated Successfully");
      // }
      // );

    }
    else{
      showToast("Fields Required");
    }

  }

  }