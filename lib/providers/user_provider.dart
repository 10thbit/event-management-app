import 'package:event_manager/models/user_model.dart';
import 'package:event_manager/services/firebase_collections.dart';
import 'package:event_manager/utilities/constants.dart';
import 'package:event_manager/utilities/routes.dart';
import 'package:event_manager/widgets/toast.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class UserProvider extends ChangeNotifier {
  String? userEmail, userID;

  final auth = FirebaseAuth.instance;
  final _firestore = FirebaseFirestore.instance;

  UserModel userModel = UserModel();
  String adminEmail = "", adminPassword = "";

  void updateUser({
    String? userEmail,
    String? userPassword,
    String? userName,
    String? userPhone,
    String? userID,
    String? adminType,
    bool? isAdmin,
    bool loading=false,
    bool isButtonActive=true,
  }) {
    userModel = UserModel(
      userEmail: userEmail ?? userModel.userEmail,
      userPassword: userPassword ?? userModel.userPassword,
      userName: userName ?? userModel.userName,
      userPhone: userPhone ?? userModel.userPhone,
      userID: userID ?? userModel.userID,
      adminType: adminType ?? userModel.adminType,
      isAdmin: isAdmin ?? userModel.isAdmin,
      loading: loading,
      isButtonActive: isButtonActive,
    );
    notifyListeners();
  }

  Future<bool> signUpAdmin() async {
    bool value = false;
    UserCredential userCredential = await auth.createUserWithEmailAndPassword(email: userModel.userEmail, password: userModel.userPassword);

    if (userCredential.user != null) {
      var userID = await auth.currentUser!.uid;
      userModel.userID = userID;
      _firestore.collection('Users').doc(userID).set(userModel.toJson());

      userEmail = userCredential.user!.email;
      userID = userCredential.user!.uid;
      value = true;
    }
    else {
      print("User not created!");
      value = false;
    }
    return value;
  }

  Future<bool> loginAdmin(String user_email, String user_password) async {
    print("Hello");
    bool retval = false;
    UserCredential userCredential = await auth.signInWithEmailAndPassword(
        email: user_email, password: user_password);
    print('-=-=- : ${userCredential}');
    if (userCredential.user != null) {
      userEmail = userCredential.user!.email;
      userID = userCredential.user!.uid;
      notifyListeners();
      return retval = true;
    }
    return retval = false;
  }

  Future<bool> checkUserType() async {
    String userId = FirebaseAuth.instance.currentUser!.uid;
    await FbCollections.user
        .doc(userId)
        .get()
        .then((DocumentSnapshot ds) {
      userModel = UserModel.fromJson(ds.data() as Map<String,dynamic>);
    });
    return userModel.isAdmin;
  }


  AdminType typeFromString(String type) {
    switch (type) {
      case AppStrings.individual:
        return AdminType.individual;
      case AppStrings.university:
        return AdminType.university;
      case AppStrings.organization:
        return AdminType.organization;
      default:
        return AdminType.individual;
    }
  }

  String stringAdminType(AdminType adminType) {
    switch (adminType) {
      case AdminType.individual:
        return AppStrings.individual;
      case AdminType.university:
        return AppStrings.university;
      case AdminType.organization:
        return AppStrings.organization;
      default:
        return AppStrings.individual;
    }
  }

  Future onLoginTapped(BuildContext context) async {
    if(adminEmail.isNotEmpty && adminPassword.isNotEmpty){
      updateUser(loading: true);
      await auth.signInWithEmailAndPassword(email: adminEmail, password: adminPassword)
          .whenComplete(
              () async {
            updateUser(loading: false);
            bool userType = await checkUserType();
            if(userType == true){
              Navigator.pushNamedAndRemoveUntil(context, Routes.adminPanelDashboard, (route) => false);
            }
            else{
              Navigator.pushNamedAndRemoveUntil(context, Routes.userDashboard, (route) => false);
            }
          }
      );
    }
    else{
      print("Fields missing");
      showToast("Fields missing");
    }
  }
}
