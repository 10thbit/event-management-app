import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:event_manager/models/events_model.dart';
import 'package:event_manager/screens/add_event.dart';
import 'package:event_manager/widgets/toast.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

final _firestore = FirebaseFirestore.instance;
final _auth = FirebaseAuth.instance;

class EventProvider extends ChangeNotifier{

  EventModel eventModel = EventModel();
  bool loading = false;

  void updateEventFields({
    String? eventID,
    String? creatorID,
    String? eventName,
    String? eventOrganizer,
    String? eventDescription,
    String? eventStartDate,
    String? eventEndDate,
    bool loading = false,
  }) {
    eventModel = EventModel(
      eventID: eventID ?? eventModel.eventID,
      creatorID: creatorID ?? eventModel.creatorID,
      eventName: eventName ?? eventModel.eventName,
      eventOrganizer: eventOrganizer ?? eventModel.eventOrganizer,
      eventDescription: eventDescription ?? eventModel.eventDescription,
      eventStartDate: eventStartDate ?? eventModel.eventStartDate,
      eventEndDate: eventEndDate ?? eventModel.eventEndDate,
    );
    notifyListeners();
  }

  Future<void> addEvent() async{
    var eventId = const Uuid().v1();
    eventModel.eventID = eventId;
    eventModel.creatorID = _auth.currentUser!.uid;
    await _firestore.collection('Events').doc(eventId).set(eventModel.toJson());
    eventModel = EventModel();
  }

  // Future<void> updateEvent(String eventId, String updateEventName, String updateEventOrganizer, String updateEventDescription, String updateEventStartDate, String updateEventEndDate, String uuid) async{
  //   final eventModel = EventModel(eventID: eventId, eventName: updateEventName, eventOrganizer: updateEventOrganizer, eventDescription: updateEventDescription, eventStartDate: updateEventStartDate, eventEndDate: updateEventEndDate, eventCode: uuid);
  //   final jsonMap = eventModel.toJson();
  //
  //   await _firestore.collection('Events').doc(eventId).update(jsonMap);
  // }

  Future<void> updateEvent(Function onUpdate) async{
    print(eventModel.eventID);
    await _firestore.collection('Events').doc(eventModel.eventID).update(eventModel.toJson());
    onUpdate();
    notifyListeners();
    // eventModel = EventModel();
  }

  Future<bool> eventExists(String userInputID) async {
    DocumentSnapshot event = await _firestore.collection('Events').doc(userInputID).get();
    return event.exists;
  }

  Future<void> onRegisterEventTapped(Function onSuccess, Function onError, EventModel eventModel) async {
    print(eventModel.eventName);
    print(eventModel.eventOrganizer);
    print(eventModel.eventDescription);
    print("Start Date: ${eventModel.eventStartDate}");
    print("End Date: ${eventModel.eventEndDate}");

    if (eventModel.eventName.isNotEmpty && eventModel.eventDescription.isNotEmpty && eventModel.eventOrganizer.isNotEmpty && eventModel.eventStartDate.isNotEmpty && eventModel.eventEndDate.isNotEmpty) {

      updateEventFields(loading: true);

      selectedStartDate = DateTime.parse(eventModel.eventStartDate);
      selectedEndDate = DateTime.parse(eventModel.eventEndDate);

      if(selectedEndDate!.isBefore(selectedStartDate!) || selectedEndDate == selectedStartDate){
        print("End date must be after start date");
        showToast("End date must be after start date");
        updateEventFields(loading: false);
      }
      else{
        showToast("Added");

        var eventId = const Uuid().v1();
        eventModel.eventID = eventId;
        eventModel.creatorID = _auth.currentUser!.uid;
        await _firestore.collection('Events').doc(eventId).set(eventModel.toJson())
            .whenComplete(() => onSuccess())
            .onError((error, stackTrace) => () => onError());


        // eventModel = EventModel();

        // addEvent()
        //     .whenComplete(
        //         () {
        //         Navigator.pushReplacementNamed(context, Routes.manageEventPage);
        //         showToast("Saved Successfully");
        //         }
        //     )
        //     .onError(
        //         (error, stackTrace) => () {
        //               showToast("Registration Failed. Try Again!");
        //         }
        //     );
        // updateEventFields(loading: false);
      }
    }
    else {
      showToast("Fields Required");
      print("Fields Missing");
      print("End Date: ${eventModel.eventEndDate}");
      print("Start Date: ${eventModel.eventStartDate}");
      print("Event Name: ${eventModel.eventName}");
      print("Event Description: ${eventModel.eventDescription}");
      print("Event Organizer: ${eventModel.eventOrganizer}");
    }
  }

  Future<void> onUpdateEventTapped(Function onSuccess, Function onError, EventModel eventModel) async {
    if(eventModel.eventName.isNotEmpty && eventModel.eventDescription.isNotEmpty &&
        eventModel.eventOrganizer.isNotEmpty && eventModel.eventStartDate.isNotEmpty &&
        eventModel.eventEndDate.isNotEmpty){

      updateEventFields(loading: true);

      print(eventModel.eventID);
      await _firestore.collection('Events').doc(eventModel.eventID).update(eventModel.toJson())
          .whenComplete(() => onSuccess())
          .onError((error, stackTrace) => onError());
      notifyListeners();

      // updateEvent((){
      //   updateEventFields(loading: false);
      //   Navigator.pop(context);
      //   showToast("Updated Successfully");
      // });

    }
    else{
      showToast("Fields Required");
    }
  }

}