import 'package:flutter/material.dart';

class UserInputTextField extends StatelessWidget {

  Function? onChangedCallback;
  String hintText;
  String? val;
  bool obscureText;
  TextInputType? keyboardType;
  TextInputAction? textInputAction;
  TextEditingController? controller;

  UserInputTextField({this.onChangedCallback, this.keyboardType, required this.textInputAction, this.obscureText=false, this.val, required this.hintText, this.controller});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 60.0, vertical: 5.0),
      child: TextFormField(
        // controller: TextEditingController(text: val ?? ""),
        controller: controller,
        obscureText: obscureText,
        keyboardType: keyboardType,
        textInputAction: textInputAction,
        onChanged: (val){
          // onChangedCallback!(val);
        },
        decoration: InputDecoration(
            filled: true,
            // hintText: hintText,
            labelText: hintText,
            hintStyle: const TextStyle(
              color: Colors.black54,
            ),

          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
            ),
          ),
          border: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red,
            ),
          ),
          disabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red,
            ),
          ),

            // border: const OutlineInputBorder(
            //   borderRadius: BorderRadius.all(
            //     Radius.circular(5.0),
            //   ),
            //   borderSide: BorderSide.none,
            // ),
            // enabledBorder: const OutlineInputBorder(
            //   borderRadius: BorderRadius.all(
            //     Radius.circular(5.0),
            //   ),
            //   borderSide: BorderSide.none,
            // ),
        ),
        // decoration: InputDecorationTextField("Enter Email"),
      ),
    );
  }

}