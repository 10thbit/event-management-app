import 'package:flutter/material.dart';

class CustomAlertDialog extends StatelessWidget {
  final Color bgColor;
  final String? title;
  final String? message;
  final String? positiveBtnText;
  final String? negativeBtnText;
  final Function? onPositivePressed;
  final Function? onNegativePressed;
  final double circularBorderRadius;

  CustomAlertDialog({
    this.title,
    this.message,
    this.circularBorderRadius = 15.0,
    this.bgColor = Colors.white,
    this.positiveBtnText,
    this.negativeBtnText,
    this.onPositivePressed,
    this.onNegativePressed,
  });

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: title != null ? Text(title!) : null,
      content: message != null ? Text(message!) : null,
      backgroundColor: bgColor,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(circularBorderRadius)
      ),
      actions: <Widget>[
        negativeBtnText != null
            ? TextButton(
            onPressed: (){
              Navigator.pop(context);
            },
            child: Text(negativeBtnText!)
            )
            : const SizedBox(),

        positiveBtnText != null
        ? TextButton(
            onPressed: (){
              Navigator.pop(context);
              if(onPositivePressed != null){
                onPositivePressed!();
              }
            },
            child: Text(
              positiveBtnText!,
              style: const TextStyle(
                color: Colors.red
              ),
              )
            )
            : const SizedBox(),
      ],
    );
  }
}