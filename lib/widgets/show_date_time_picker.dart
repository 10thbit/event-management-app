import 'package:date_time_picker/date_time_picker.dart';
import 'package:event_manager/providers/event_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class showDateTimePicker extends StatelessWidget {

  String? labelText;
  String? val;
  bool isEnabled;
  Function changed;
  DateTime? date;


  showDateTimePicker({
    this.val,
    this.labelText,
    this.isEnabled = true,
    required this.changed,
    this.date,
  });

  @override
  Widget build(BuildContext context) {
    return Consumer<EventProvider>(
      builder: (context, eventProvider, child) {
        return Container(
          margin: const EdgeInsets.symmetric(horizontal: 60, vertical: 5),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Colors.grey.shade200
          ),
          child: DateTimePicker(
            controller: TextEditingController(text: val),
            type: DateTimePickerType.dateTime,
            dateMask: 'd MMM, yyyy hh:mm',
            // initialValue: DateTime.now().toString(),
            // initialDate: date,
            firstDate: date ?? DateTime.now(),
            //firstDate: date,
            // firstDate: eventProvider.eventModel.eventStartDate.isEmpty ? eventProvider.eventModel.eventStartDate as DateTime : DateTime.now(),
            lastDate: DateTime(2100),
            //enabled: isEnabled,
            dateLabelText: labelText,
            onChanged: (value) {
              print(value);
              // controller!.text = value;
              changed(value);
            },
          ),
        );
      },
    );
  }
}