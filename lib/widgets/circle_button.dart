import 'package:flutter/material.dart';

class CircleButton extends StatelessWidget {

  Color? buttonColor;
  IconData? icon;
  double? iconRadius, iconSize;

  CircleButton({required this.buttonColor, required this.icon, this.iconRadius, required this.iconSize});

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: buttonColor,
      radius: iconRadius,
      child: Icon(
        icon,
        size: iconSize,
        color: Colors.white,
      ),
    );
  }
}
