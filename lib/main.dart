import 'package:event_manager/providers/event_provider.dart';
import 'package:event_manager/providers/sub_event_provider.dart';
import 'package:event_manager/providers/user_provider.dart';
import 'package:event_manager/screens/add_event.dart';
import 'package:event_manager/screens/add_sub_event.dart';
import 'package:event_manager/screens/admin_dashboard.dart';
import 'package:event_manager/screens/admin_panel_login_screen.dart';
import 'package:event_manager/screens/admin_panel_register_screen.dart';
import 'package:event_manager/screens/authentication_screen.dart';
import 'package:event_manager/screens/event_view.dart';
import 'package:event_manager/screens/manage_events.dart';
import 'package:event_manager/screens/sub_event_view.dart';
import 'package:event_manager/screens/update_event.dart';
import 'package:event_manager/screens/update_sub_event.dart';
import 'package:event_manager/screens/user_dashboard.dart';
import 'package:event_manager/utilities/routes.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => UserProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => EventProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => SubEventProvider(),
        ),
      ],
      child: GetMaterialApp(
        initialRoute: Routes.authenticationScreen,
        routes: {
          Routes.authenticationScreen: (_) => AuthenticationScreen(),
          Routes.userDashboard: (context) => UserDashboard(),
          Routes.adminPanelLoginScreen: (context) => AdminPanelLogin(),
          Routes.adminPanelRegisterScreen: (context) => AdminPanelRegisterScreen(),
          Routes.adminPanelDashboard: (context) => AdminDashboard(),
          Routes.addEventPage: (context) => AddEvent(),
          Routes.manageEventPage: (context) => ManageEvents(),
          Routes.eventViewPage: (context) => EventView(),
          Routes.subEventViewPage: (context) => SubEventView(),
          Routes.addSubEventPage: (context) => AddSubEvent(),
          Routes.updateEventPage: (context) => UpdateEvent(),
          Routes.updateSubEventPage: (context) => UpdateSubEvent(),
        },
      ),
    );
  }
}
