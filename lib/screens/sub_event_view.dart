import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:event_manager/models/events_model.dart';
import 'package:event_manager/models/sub_events_model.dart';
import 'package:event_manager/providers/event_provider.dart';
import 'package:event_manager/providers/sub_event_provider.dart';
import 'package:event_manager/utilities/routes.dart';
import 'package:event_manager/widgets/circle_button.dart';
import 'package:event_manager/widgets/custom_alert_dialog.dart';
import 'package:event_manager/widgets/toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class SubEventView extends StatelessWidget {

  final _firebase = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    return Consumer2<EventProvider, SubEventProvider>(builder: (context, eventProvider, subEventProvider, child) {
      return Scaffold(
        body: Container(
          margin: const EdgeInsets.only(top: 100.0),
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children:
            [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  TextButton.icon(
                    onPressed: (){
                      Navigator.pop(context);
                      // eventProvider.eventModel = EventModel();
                      // subEventProvider.subEventModel = SubEventsModel();
                    },
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.grey.shade400),
                    ),
                    label: const Text(
                      "Go back",
                      style: TextStyle(
                          color: Colors.white
                      ),
                    ),
                    icon: const Icon(
                      Icons.arrow_back_rounded,
                      color: Colors.white,
                    ),
                  ),
                  Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          const SizedBox(width: 10),
                          GestureDetector(
                            onTap: (){
                              Navigator.pushNamed(context, Routes.updateSubEventPage);
                            },
                            child: CircleButton(
                              buttonColor: Colors.lightBlue,
                              icon: Icons.edit,
                              iconSize: 23,
                            ),
                          ),
                          const SizedBox(width: 10),
                          GestureDetector(
                            onTap: (){
                              var dialog = CustomAlertDialog(
                                  title: "Delete Sub Event",
                                  message: "Confirm Delete Sub-Event?",
                                  onPositivePressed: () {
                                    _firebase.collection('Events').doc(eventProvider.eventModel.eventID).collection('SubEvents').doc(subEventProvider.subEventModel.subEventID).delete();
                                    Navigator.pop(context);
                                    showToast("Deleted Successfully");
                                  },
                                  positiveBtnText: 'Delete',
                                  negativeBtnText: 'Dismiss'
                              );
                              showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) => dialog
                              );
                            },
                            child: CircleButton(
                              buttonColor: Colors.red,
                              icon: Icons.delete_rounded,
                              iconSize: 23,
                            ),
                          ),
                        ],
                      )
                  ),
                ],
              ),
              const SizedBox(height: 10),
              Text(
                subEventProvider.subEventModel.subEventName,
                style: const TextStyle(
                    fontSize: 35.0,
                    color: Colors.black
                ),
              ),
              const SizedBox(height: 20.0),
              const Text(
                "Category",
                style: TextStyle(
                    fontSize: 13.0,
                    color: Colors.black54
                ),
              ),
              Text(
                subEventProvider.subEventModel.subEventCategory,
                style: const TextStyle(
                    fontSize: 25.0,
                    color: Colors.black
                ),
              ),
              const SizedBox(height: 20.0),
              const Text(
                "Person Allowed",
                style: TextStyle(
                    fontSize: 13.0,
                    color: Colors.black54
                ),
              ),
              Row(
                children: [
                  Text(
                    subEventProvider.subEventModel.subEventsPersons,
                    style: const TextStyle(
                        fontSize: 15.0,
                        color: Colors.black
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 20.0),
              const Text(
                "Event Fee",
                style: TextStyle(
                    fontSize: 13.0,
                    color: Colors.black54
                ),
              ),
              Text(
                subEventProvider.subEventModel.subEventFee,
                style: const TextStyle(
                    fontSize: 15.0,
                    color: Colors.black
                ),
              ),
              const SizedBox(height: 20.0),
              const Text(
                "Description",
                style: TextStyle(
                    fontSize: 13.0,
                    color: Colors.black54
                ),
              ),
              Text(
                subEventProvider.subEventModel.subEventDescription,
                style: const TextStyle(
                    fontSize: 15.0,
                    color: Colors.black
                ),
              ),
            ],
          ),
        ),
      );
    },
    );
  }
}
