import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:event_manager/models/events_model.dart';
import 'package:event_manager/models/sub_events_model.dart';
import 'package:event_manager/providers/event_provider.dart';
import 'package:event_manager/providers/sub_event_provider.dart';
import 'package:event_manager/utilities/routes.dart';
import 'package:event_manager/widgets/circle_button.dart';
import 'package:event_manager/widgets/custom_alert_dialog.dart';
import 'package:event_manager/widgets/toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';


class EventView extends StatefulWidget {
  EventView({Key? key}) : super(key: key);

  @override
  _EventViewState createState() => _EventViewState();
}

class _EventViewState extends State<EventView> {

  final _firebase = FirebaseFirestore.instance;
  bool shouldPop = true;

  @override
  Widget build(BuildContext context) {
    return Consumer2<EventProvider, SubEventProvider>(builder: (context, eventProvider, subEventProvider, child) {
        return WillPopScope(
          onWillPop: () async{
            eventProvider.eventModel = EventModel();
            return shouldPop;
          },
          child: Scaffold(
            body: Container(
              margin: const EdgeInsets.only(top: 100.0),
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children:
                [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      TextButton.icon(
                        onPressed: (){
                          Navigator.pop(context);
                          eventProvider.eventModel = EventModel();
                          subEventProvider.subEventModel = SubEventsModel();
                        },
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(Colors.grey.shade400),
                        ),
                        label: const Text(
                          "Go back",
                          style: TextStyle(
                              color: Colors.white
                          ),
                        ),
                        icon: const Icon(
                          Icons.arrow_back_rounded,
                          color: Colors.white,
                        ),
                      ),
                      Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              GestureDetector(
                                onTap: (){
                                  Navigator.pushNamed(context, Routes.addSubEventPage);
                                },
                                child: CircleButton(
                                  buttonColor: Colors.green.shade700,
                                  icon: Icons.calendar_today_rounded,
                                  iconSize: 22,
                                ),
                              ),
                              const SizedBox(width: 10),
                              GestureDetector(
                                onTap: (){
                                  Navigator.pushNamed(context, Routes.updateEventPage);
                                },
                                child: CircleButton(
                                  buttonColor: Colors.lightBlue,
                                  icon: Icons.edit,
                                  iconSize: 23,
                                ),
                              ),
                              const SizedBox(width: 10),
                              GestureDetector(
                                onTap: (){
                                  var dialog = CustomAlertDialog(
                                      title: "Delete Event",
                                      message: "It will delete the event and its all subevents permanently. Delete Event?",
                                      onPositivePressed: () {
                                        print("Deleted");
                                        _firebase.collection('Events').doc(eventProvider.eventModel.eventID).delete();
                                        Get.back();
                                        showToast("Deleted Successfully");
                                      },
                                      positiveBtnText: 'Delete',
                                      negativeBtnText: 'Dismiss'
                                  );
                                  showDialog(
                                      context: context,
                                      barrierDismissible: false,
                                      builder: (BuildContext context) => dialog
                                  );
                                },
                                child: CircleButton(
                                  buttonColor: Colors.red,
                                  icon: Icons.delete_rounded,
                                  iconSize: 23,
                                ),
                              ),
                            ],
                          )
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  Text(
                    eventProvider.eventModel.eventName,
                    style: const TextStyle(
                        fontSize: 35.0,
                        color: Colors.black
                    ),
                  ),
                  const SizedBox(height: 20.0),
                  const Text(
                    "Organized by",
                    style: TextStyle(
                        fontSize: 13.0,
                        color: Colors.black54
                    ),
                  ),
                  Text(
                    eventProvider.eventModel.eventOrganizer,
                    style: const TextStyle(
                        fontSize: 25.0,
                        color: Colors.black
                    ),
                  ),
                  const SizedBox(height: 20.0),
                  const Text(
                    "Event Join Code",
                    style: TextStyle(
                        fontSize: 13.0,
                        color: Colors.black54
                    ),
                  ),
                  Row(
                    children: [
                      Text(
                        eventProvider.eventModel.eventID,
                        style: const TextStyle(
                            fontSize: 15.0,
                            color: Colors.black
                        ),
                      ),
                      IconButton(
                        constraints: const BoxConstraints(),
                        padding: const EdgeInsets.only(left: 10),
                        onPressed: (){
                          Clipboard.setData(
                              ClipboardData(
                                  text: "Join Event Invitation\n\nEvent Name: ${eventProvider.eventModel.eventName}\n\nJoin Event using this code: ${eventProvider.eventModel.eventID}"
                              )
                          );
                          showToast("Copied to Clipboard!");
                        },
                        icon: const Icon(
                            Icons.copy,
                            color: Colors.blue
                        ),
                        iconSize: 18,
                      ),
                    ],
                  ),
                  const SizedBox(height: 20.0),
                  const Text(
                    "Start Date",
                    style: TextStyle(
                        fontSize: 13.0,
                        color: Colors.black54
                    ),
                  ),
                  Text(
                    eventProvider.eventModel.eventStartDate,
                    style: const TextStyle(
                        fontSize: 15.0,
                        color: Colors.black
                    ),
                  ),
                  const SizedBox(height: 20.0),
                  const Text(
                    "End Date",
                    style: TextStyle(
                        fontSize: 13.0,
                        color: Colors.black54
                    ),
                  ),
                  Text(
                    eventProvider.eventModel.eventEndDate,
                    style: const TextStyle(
                        fontSize: 15.0,
                        color: Colors.black
                    ),
                  ),
                  const SizedBox(height: 20.0),
                  const Text(
                    "Description",
                    style: TextStyle(
                        fontSize: 13.0,
                        color: Colors.black54
                    ),
                  ),
                  Text(
                    eventProvider.eventModel.eventDescription,
                    style: const TextStyle(
                        fontSize: 15.0,
                        color: Colors.black
                    ),
                  ),
                  const SizedBox(height: 5),
                  const Divider(),
                  const Text(
                    "Sub Events",
                    style: TextStyle(
                        color: Colors.black54
                    ),
                  ),

                  Expanded(
                    child: StreamBuilder<QuerySnapshot>(
                      stream: _firebase.collection('Events').doc(eventProvider.eventModel.eventID).collection('SubEvents').snapshots(),
                      builder: (context, snapshot){
                        if(!snapshot.hasData){
                          return const Center(
                            child: Text("There are no sub-events available."),
                            // CircularProgressIndicator(
                            //   backgroundColor: Colors.grey,
                            // ),
                          );
                        }
                        List<SubEventsModel> subEventsList = List.empty(growable: true);
                        snapshot.data!.docs.forEach((element) {
                          subEventsList.add(
                              SubEventsModel.fromJson(element.data() as Map<String, dynamic>)
                          );
                        });
                        if (subEventsList.isEmpty) {
                          return const Center(
                            child: Text("There are no sub-events available."),
                            // CircularProgressIndicator(
                            //   backgroundColor: Colors.grey,
                            // ),
                          );
                        }
                        return ListView.builder(
                          shrinkWrap: true,
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          itemCount: subEventsList.length,
                          itemBuilder: (context, index) {
                            SubEventsModel subEventModel = subEventsList[index];

                            return GestureDetector(
                              onTap: (){
                                subEventProvider.subEventModel = subEventModel;
                                Navigator.pushNamed(context, Routes.subEventViewPage);
                              },
                              child: Container(
                                margin: const EdgeInsets.symmetric(vertical: 2.0),
                                child: Card(
                                  child: Container(
                                    padding: const EdgeInsets.all(10),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(subEventModel.subEventName, style: const TextStyle(fontSize: 20.0),),
                                        const SizedBox(height: 5.0),
                                        Text("Category: ${subEventModel.subEventCategory}", style: const TextStyle(color: Colors.black54),),
                                        const SizedBox(height: 5.0),
                                        Row(
                                          children: [
                                            CircleButton(
                                              buttonColor: Colors.lightBlue,
                                              icon: Icons.supervisor_account_rounded,
                                              iconRadius: 12,
                                              iconSize: 15,
                                            ),
                                            const SizedBox(width: 5),
                                            Text(subEventModel.subEventsPersons),
                                            const SizedBox(width: 20),
                                            CircleButton(
                                              buttonColor: Colors.green,
                                              icon: Icons.attach_money_outlined,
                                              iconRadius: 12,
                                              iconSize: 15,
                                            ),
                                            const SizedBox(width: 5),
                                            Text(subEventModel.subEventFee),
                                            const SizedBox(width: 20),
                                            CircleButton(
                                              buttonColor: Colors.orangeAccent,
                                              icon: Icons.category_rounded,
                                              iconRadius: 12,
                                              iconSize: 15,
                                            ),
                                            const SizedBox(width: 5),
                                            Text(subEventModel.subEventCategory),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      },
                    ),
                  )

                ],
              ),
            ),
          ),
        );
      },
    );
  }
}