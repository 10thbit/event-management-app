import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:event_manager/providers/user_provider.dart';
import 'package:event_manager/services/firebase_collections.dart';
import 'package:event_manager/utilities/routes.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

final _auth = FirebaseAuth.instance;
String uid = FirebaseAuth.instance.currentUser!.uid;

DocumentReference databaseReference =
    FirebaseFirestore.instance.collection("Users").doc(uid);

class AdminDashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<UserProvider>(
      builder: (context, userProvider, child) {
        return Scaffold(
          body: Container(
            margin: const EdgeInsets.only(top: 60.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                // UserManagement().getData(),
                const SizedBox(height: 30.0),
                const Center(
                  child: Text(
                    "Admin Panel",
                    style: TextStyle(
                      fontSize: 20.0,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 25.0, top: 20.0),
                  child: Row(
                    children: [
                      const Text("Logged in as:"),
                      const SizedBox(width: 5.0),
                      Text("${_auth.currentUser!.email}"),
                      // Text(userProvider.getUserData()),
                      GestureDetector(
                          onTap: () {
                            _auth.signOut();
                            print("sign out tapped");
                            Navigator.pushNamedAndRemoveUntil(context, Routes.adminPanelLoginScreen, (route) => false);
                          },
                          child: const Text(
                            " (sign out)",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          )),
                    ],
                  ),
                ),
                const SizedBox(height: 30.0),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, Routes.addEventPage);
                          },
                          child: Card(
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12.0, vertical: 25.0),
                              child: Column(
                                children: [
                                  Hero(
                                      tag: "add_event",
                                      child: Image.asset(
                                          "images/add_event.png",
                                          scale: 3)),
                                  const SizedBox(height: 10.0),
                                  const Text("Add Event"),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 10.0),
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, Routes.manageEventPage);
                          },
                          child: Card(
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12.0, vertical: 25.0),
                              child: Column(
                                children: [
                                  Hero(
                                    tag: "manage_events",
                                    child: Image.asset(
                                      "images/manage_event.png",
                                      scale: 3.5,
                                    ),
                                  ),
                                  const SizedBox(height: 10.0),
                                  const Text("Manage Events")
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 5.0),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Card(
                    elevation: 50.0,
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 12.0, vertical: 20.0),
                      child: Row(
                        children: const [
                          Icon(
                            Icons.add_alert,
                            color: Colors.blue,
                          ),
                          SizedBox(width: 10.0),
                          Text("Add announcements")
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class UserManagement {
  getData() async {
    String userId = await FirebaseAuth.instance.currentUser!.uid;
    print(userId);
    return FbCollections.user
        .doc(userId)
        .get()
        .then((DocumentSnapshot ds) {
      print(ds['isAdmin']);
    });
  }
}
