import 'package:event_manager/models/events_model.dart';
import 'package:event_manager/providers/event_provider.dart';
import 'package:event_manager/utilities/routes.dart';
import 'package:event_manager/widgets/show_date_time_picker.dart';
import 'package:event_manager/widgets/toast.dart';
import 'package:event_manager/widgets/user_input_textField.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';
import 'package:intl/intl.dart';

var uuid = const Uuid();
DateTime? selectedStartDate;
DateTime? selectedEndDate;
bool? isEnabledEndDateTime;
var myFormat = DateFormat("dd-MM-yyyy hh:mm:ss");

class AddEvent extends StatefulWidget {
  @override
  _AddEventState createState() => _AddEventState();
}

class _AddEventState extends State<AddEvent> {
  // String? eventName, eventOrganizer, eventStartDate, eventEndDate,
  //     eventDescription;

  final eventStartDateTextController = TextEditingController();
  final eventEndDateTextController = TextEditingController();
  bool shouldPop = true;

  final nameTextController = TextEditingController();
  final organizerTextController = TextEditingController();
  final descriptionTextController = TextEditingController();
  String startDate = "";
  String endDate = "";

  @override
  Widget build(BuildContext context) {
    return Consumer<EventProvider>(
      builder: (context, eventProvider, child) {
        return WillPopScope(
          onWillPop: () async {
            return shouldPop;
          },
          child: Scaffold(
            body: SafeArea(
              child: Center(
                child: SingleChildScrollView(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Hero(
                            tag: "add_event",
                            child:
                                Image.asset("images/add_event.png", scale: 3)),
                        const SizedBox(
                          height: 20.0,
                        ),
                        const Text(
                          "Register new Event",
                          style: TextStyle(fontSize: 20.0),
                        ),
                        const SizedBox(height: 40.0),
                        UserInputTextField(
                          // onChangedCallback: (newEventName) {
                          //   // eventProvider.updateEventFields(eventName: newEventName);
                          //   eventProvider.eventModel.eventName= newEventName;
                          // },
                          controller: nameTextController,
                          hintText: "Event Name",
                          textInputAction: TextInputAction.next,
                        ),
                        UserInputTextField(
                          // onChangedCallback: (newOrganizer) {
                          //   eventProvider.updateEventFields(eventOrganizer: newOrganizer);
                          //   // eventProvider.eventModel.eventOrganizer = newOrganizer;
                          // },
                          controller: organizerTextController,
                          hintText: "Event Organizer",
                          textInputAction: TextInputAction.next,
                        ),
                        showDateTimePicker(
                          // val: eventProvider.eventModel.eventStartDate,
                          val: startDate,
                          labelText: "Event Start Date",
                          changed: (value) {
                            // eventProvider.updateEventFields(eventStartDate: value);
                            // eventProvider.eventModel.eventStartDate = value;
                            startDate = value;
                          },
                        ),
                        showDateTimePicker(
                          // val: eventProvider.eventModel.eventEndDate,
                          val: endDate,
                          labelText: "Event End Date",
                          changed: (value) {
                            // eventProvider.updateEventFields(eventEndDate: value);
                            // eventProvider.eventModel.eventEndDate = value;
                            endDate = value;
                          },
                        ),
                        UserInputTextField(
                          // onChangedCallback: (newDescription) {
                          //   // eventProvider.updateEventFields(eventDescription: newDescription);
                          //   eventProvider.eventModel.eventDescription = newDescription;
                          //   // eventProvider.eventModel.toJson();
                          // },
                          controller: descriptionTextController,
                          hintText: "Event Description",
                          textInputAction: TextInputAction.done,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 60.0),
                                child: MaterialButton(
                                  onPressed: () {
                                    final eventModel = EventModel(
                                        eventName: nameTextController.text,
                                        eventOrganizer:
                                            organizerTextController.text,
                                        eventDescription:
                                            descriptionTextController.text,
                                        eventStartDate: startDate,
                                        eventEndDate: endDate);
                                    // eventModel.eventName = nameTextController.text;
                                    // eventModel.eventOrganizer = organizerTextController.text;
                                    // eventModel.eventDescription = descriptionTextController.text;
                                    // eventModel.eventName = nameTextController.text;
                                    // eventModel.eventName = nameTextController.text;
                                    // eventModel.toJson();
                                    eventProvider.onRegisterEventTapped(
                                      (){
                                        Navigator.pushReplacementNamed(
                                            context, Routes.manageEventPage);
                                        showToast("Saved Successfully");
                                      },
                                      (){
                                        showToast("Registration Failed. Try Again!");
                                      },
                                      eventModel,
                                    );
                                  },
                                  color: Colors.blue,
                                  child: const Text(
                                    "Register Event",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Visibility(
                          visible: eventProvider.loading,
                          child: Container(
                            width: 30,
                            height: 30,
                            margin: const EdgeInsets.only(top: 20),
                            child: const CircularProgressIndicator(
                              backgroundColor: Colors.grey,
                            ),
                          ),
                        ),
                      ]),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
