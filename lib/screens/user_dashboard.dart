import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:event_manager/models/events_model.dart';
import 'package:event_manager/models/sub_events_model.dart';
import 'package:event_manager/providers/event_provider.dart';
import 'package:event_manager/providers/user_provider.dart';
import 'package:event_manager/widgets/toast.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class UserDashboard extends StatelessWidget {

  String userName="";
  String userInputCode="";
  // final auth = FirebaseAuth.instance;
  final _firestore = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    return Consumer2<UserProvider, EventProvider>(builder: (context, userProvider, eventProvider, child){
      return Scaffold(
        body: SafeArea(
          child: Container(
            margin: const EdgeInsets.only(top: 30, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "Welcome",
                      style: TextStyle(
                          fontSize: 30,
                          color: Colors.blue
                      ),
                    ),
                    IconButton(
                        onPressed: (){
                          userProvider.auth.signOut();
                          Navigator.pushNamedAndRemoveUntil(context, 'AdminPanelLogin', (route) => false);
                        },
                        icon: const Icon(
                          Icons.logout,
                          color: Colors.red
                        )
                    )
                  ],
                ),
                Text(userProvider.userModel.userName),
                StreamBuilder<QuerySnapshot>(
                  stream: _firestore.collection('Users').doc(userProvider.auth.currentUser!.uid).collection('Registered Events').snapshots(),
                  builder: (context, snapshot){
                    if(snapshot.connectionState == ConnectionState.done && !snapshot.hasData){
                      return Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(top: 20, left: 10, right: 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.grey.shade200,
                            ),
                            child: ListTile(
                              leading: const Icon(
                                Icons.search,
                                color: Colors.black54,
                                size: 28,
                              ),
                              title: TextField(
                                onChanged: (enteredCode){
                                  userInputCode = enteredCode;
                                },
                                decoration: const InputDecoration(
                                  hintText: 'enter invite code to join',
                                  hintStyle: TextStyle(
                                    color: Colors.black54,
                                    fontSize: 18,
                                    fontStyle: FontStyle.italic,
                                  ),
                                  border: InputBorder.none,
                                ),
                                style: const TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(height: 5),
                          Center(
                            child: TextButton(
                              onPressed: () async {
                                if(userInputCode.isNotEmpty){
                                  bool eventExists = await eventProvider.eventExists(userInputCode);
                                  print(eventExists);
                                  if (eventExists){
                                    _firestore.collection('Events').doc(userInputCode).collection('Registered Users').doc(userProvider.auth.currentUser!.uid).set({"Registered User Id":userProvider.auth.currentUser!.uid});
                                    _firestore.collection('Users').doc(userProvider.auth.currentUser!.uid).collection('Registered Events').doc(userInputCode).set({"Registered Events":userInputCode});
                                    showToast("Added Successfully");
                                  }
                                  else{
                                    showToast("No event found with this id!");
                                  }
                                }
                                else{
                                  print("Field Missing");
                                  showToast("Field Missing");
                                }
                              },
                              child: const Text(
                                "Join Event",
                                style: TextStyle(
                                    color: Colors.white
                                ),
                              ),
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Colors.blue)
                              ),
                            ),
                          )
                        ],
                      );
                    }
                    // if(snapshot.hasData){
                    // List<String> registeredEvents = snapshot.data!.docs.map((e) => jsonDecode(e.data().toString()).toString()).toList();
                    // for(String eve in registeredEvents){
                    //   print(eve);
                    // }}

                    List<EventModel> eventsList = List.empty(growable: true);
                    if(snapshot.hasData){
                      snapshot.data!.docs.forEach((element) {
                        eventsList.add(
                            EventModel.fromJson(element.data() as Map<String, dynamic>)
                        );
                      });
                      if (eventsList.isEmpty) {
                        return Column(
                          children: [
                            Container(
                              margin: const EdgeInsets.only(top: 20, left: 10, right: 10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.grey.shade200,
                              ),
                              child: ListTile(
                                leading: const Icon(
                                  Icons.search,
                                  color: Colors.black54,
                                  size: 28,
                                ),
                                title: TextField(
                                  onChanged: (enteredCode){
                                    userInputCode = enteredCode;
                                  },
                                  decoration: const InputDecoration(
                                    hintText: 'enter invite code to join',
                                    hintStyle: TextStyle(
                                      color: Colors.black54,
                                      fontSize: 18,
                                      fontStyle: FontStyle.italic,
                                    ),
                                    border: InputBorder.none,
                                  ),
                                  style: const TextStyle(
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(height: 5),
                            Center(
                              child: TextButton(
                                onPressed: () async {
                                  if(userInputCode.isNotEmpty){
                                    bool eventExists = await eventProvider.eventExists(userInputCode);
                                    print(eventExists);
                                    if (eventExists){
                                      _firestore.collection('Events').doc(userInputCode).collection('Registered Users').doc(userProvider.auth.currentUser!.uid).set({"Registered User Id":userProvider.auth.currentUser!.uid});
                                      _firestore.collection('Users').doc(userProvider.auth.currentUser!.uid).collection('Registered Events').doc(userInputCode).set({"Registered Events":userInputCode});
                                      showToast("Added Successfully");
                                    }
                                    else{
                                      showToast("No event found with this id!");
                                    }
                                  }
                                  else{
                                    print("Field Missing");
                                    showToast("Field Missing");
                                  }
                                },
                                child: const Text(
                                  "Join Event",
                                  style: TextStyle(
                                      color: Colors.white
                                  ),
                                ),
                                style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(Colors.blue)
                                ),
                              ),
                            )
                          ],
                        );
                      }
                    }
                    return ListView.builder(
                      shrinkWrap: true,
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      itemCount: eventsList.length,
                      itemBuilder: (context, index) {
                        EventModel eventModel = eventsList[index];

                        return GestureDetector(
                          onTap: (){
                            // subEventProvider.subEventModel = subEventModel;
                            // Navigator.pushNamed(context, Routes.subEventViewPage);
                          },
                          child: Container(
                            margin: const EdgeInsets.symmetric(vertical: 2.0),
                            child: Card(
                              child: Container(
                                padding: const EdgeInsets.all(10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(eventModel.eventID, style: const TextStyle(fontSize: 20.0),),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      );
    },
    );
  }
}
