import 'package:event_manager/models/events_model.dart';
import 'package:event_manager/providers/event_provider.dart';
import 'package:event_manager/widgets/show_date_time_picker.dart';
import 'package:event_manager/widgets/toast.dart';
import 'package:event_manager/widgets/user_input_textField.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

final _firestore = FirebaseFirestore.instance;
DateTime selectedStartDate = DateTime.now();
DateTime selectedEndDate = DateTime.now();
var myFormat = DateFormat("dd-MM-yyyy");

class UpdateEvent extends StatefulWidget {

  @override
  _UpdateEventState createState() => _UpdateEventState();
}

class _UpdateEventState extends State<UpdateEvent> {

  String? updateEventName, updateEventOrganizer, updateEventStartDate, updateEventEndDate, updateEventDescription;

  TextEditingController updateEventStartDateTextController = TextEditingController();
  TextEditingController updateEventEndDateTextController = TextEditingController();

  final updateNameTextController = TextEditingController();
  final updateOrganizerTextController = TextEditingController();
  final updateDescriptionTextController = TextEditingController();
  // String updateStartDate = "";
  // String updateEndDate = "";


  @override
  Widget build(BuildContext context) {
    return Consumer<EventProvider>(builder: (context, eventProvider, child) {
      updateNameTextController.text = eventProvider.eventModel.eventName;
      updateOrganizerTextController.text = eventProvider.eventModel.eventOrganizer;
      updateDescriptionTextController.text = eventProvider.eventModel.eventDescription;
      updateEventStartDateTextController.text = eventProvider.eventModel.eventStartDate;
      updateEventEndDateTextController.text = eventProvider.eventModel.eventEndDate;

      String updateStartDate = eventProvider.eventModel.eventStartDate;
      String updateEndDate = eventProvider.eventModel.eventEndDate;

      return Scaffold(
        body: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Hero(tag: "add_event",
                        child: Image.asset(
                            "images/add_event.png",
                            scale: 3
                        )
                    ),
                    const SizedBox(height: 20.0,),
                    const Text("Modify Event Details", style: TextStyle(fontSize: 20.0),),
                    const SizedBox(height: 40.0),
                    Text(
                      "Code: ${eventProvider.eventModel.eventID}",
                      style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    const SizedBox(height: 10.0),
                    UserInputTextField(
                      controller: updateNameTextController,
                      // onChangedCallback: (newEventName){
                      //   eventProvider.eventModel.eventName = newEventName;
                      // },
                      hintText: "Event Name",
                      textInputAction: TextInputAction.next,
                    ),
                    UserInputTextField(
                      // val: eventProvider.eventModel.eventOrganizer,
                      controller: updateOrganizerTextController,
                      // onChangedCallback: (newOrganizer){
                      //   eventProvider.eventModel.eventOrganizer = newOrganizer;
                      // },
                      hintText: "Event Organizer",
                      textInputAction: TextInputAction.next,
                    ),
                    showDateTimePicker(
                      val: updateStartDate,
                      labelText: "Event Start Date",
                      date: DateTime(1971),
                      changed: (value) {
                        // eventProvider.eventModel.eventStartDate = value;
                        updateStartDate = value;
                      },
                    ),
                    showDateTimePicker(
                      val: updateEndDate,
                      labelText: "Event End Date",
                      date: DateTime(1971),
                      changed: (value) {
                        // eventProvider.eventModel.eventEndDate = value;
                        updateEndDate = value;
                      },
                    ),
                    UserInputTextField(
                      controller: updateDescriptionTextController,
                      // onChangedCallback: (newDescription){
                      //   eventProvider.eventModel.eventDescription = newDescription;
                      // },
                      hintText: "Event Description",
                      textInputAction: TextInputAction.done,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.symmetric(horizontal: 60.0),
                            child: MaterialButton(
                              onPressed: (){
                                final eventModel = EventModel(
                                    creatorID: eventProvider.eventModel.creatorID,
                                    eventID: eventProvider.eventModel.eventID,
                                    eventName: updateNameTextController.text,
                                    eventOrganizer: updateOrganizerTextController.text,
                                    eventDescription: updateDescriptionTextController.text,
                                    eventStartDate: updateStartDate,
                                    eventEndDate: updateEndDate);

                                eventProvider.onUpdateEventTapped(
                                    (){
                                      eventProvider.updateEventFields(loading: false);
                                      Navigator.pop(context);
                                      showToast("Updated Successfully");
                                    },
                                    (){
                                      eventProvider.updateEventFields(loading: false);
                                      showToast("Updated Failed");
                                    },
                                    eventModel
                                );
                              },
                              color: Colors.blue,
                              child: const Text(
                                "Update Event",
                                style: TextStyle(
                                    color: Colors.white
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Visibility(
                      visible: eventProvider.loading,
                      child: Container(
                        width: 30,
                        height: 30,
                        margin: const EdgeInsets.only(top: 20),
                        child: const CircularProgressIndicator(
                          backgroundColor: Colors.grey,
                        ),
                      ),
                    ),
                  ]
              ),
            ),
          ),
        ),
      );
    },

    );


  }

  // _selectStartDate(BuildContext context) async {
  //   final DateTime? selected = await showDatePicker(
  //     context: context,
  //     initialDate: selectedStartDate,
  //     firstDate: DateTime(2010),
  //     lastDate: DateTime(2025),
  //
  //   );
  //   if (selected != null && selected != selectedStartDate) {
  //     setState(() {
  //       selectedStartDate = selected;
  //       // eventStartDateTextController.text = selected.toString();
  //       updateEventStartDateTextController.text = myFormat.format(selectedStartDate);
  //     });
  //   }
  // }

}
