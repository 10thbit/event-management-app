import 'package:event_manager/models/sub_events_model.dart';
import 'package:event_manager/providers/event_provider.dart';
import 'package:event_manager/providers/sub_event_provider.dart';
import 'package:event_manager/widgets/toast.dart';
import 'package:event_manager/widgets/user_input_textField.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class UpdateSubEvent extends StatelessWidget {
  UpdateSubEvent({Key? key}) : super(key: key);

  final updateSubEventNameTextController = TextEditingController();
  final updateSubEventCategoryTextController = TextEditingController();
  final updateSubEventFeeTextController = TextEditingController();
  final updateSubEventPersonsTextController = TextEditingController();
  final updateSubEventDescriptionTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Consumer2<EventProvider, SubEventProvider>(builder: (context, eventProvider, subEventProvider, _){

      updateSubEventNameTextController.text = subEventProvider.subEventModel.subEventName;
      updateSubEventCategoryTextController.text = subEventProvider.subEventModel.subEventCategory;
      updateSubEventFeeTextController.text = subEventProvider.subEventModel.subEventFee;
      updateSubEventPersonsTextController.text = subEventProvider.subEventModel.subEventsPersons;
      updateSubEventDescriptionTextController.text = subEventProvider.subEventModel.subEventDescription;

      return Scaffold(
        body: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                        "images/add_event.png",
                        scale: 3
                    ),
                    const SizedBox(height: 20.0,),
                    const Text("Modify Sub Event Details", style: TextStyle(fontSize: 20.0),),
                    const SizedBox(height: 10),
                    Text("Main Event: ${eventProvider.eventModel.eventName}"),
                    const SizedBox(height: 40.0),
                    UserInputTextField(
                      // val: subEventProvider.subEventModel.subEventName,
                      // onChangedCallback: (newSubEventName){
                      //   // subEventProvider.subEventModel.subEventName = newSubEventName;
                      //   subEventProvider.updateSubEventFields(subEventName: newSubEventName);
                      // },
                      controller: updateSubEventNameTextController,
                      hintText: "Sub Event Name",
                      textInputAction: TextInputAction.next,
                    ),
                    UserInputTextField(
                      // val: subEventProvider.subEventModel.subEventCategory,
                      // onChangedCallback: (newSubEventCategory){
                      //   // subEventProvider.subEventModel.subEventCategory = newSubEventCategory;
                      //   subEventProvider.updateSubEventFields(subEventCategory: newSubEventCategory);
                      // },
                      controller: updateSubEventCategoryTextController,
                      hintText: "Sub Event Category",
                      textInputAction: TextInputAction.next,
                    ),
                    UserInputTextField(
                      // val: subEventProvider.subEventModel.subEventFee,
                      // onChangedCallback: (newSubEventFee){
                      //   // subEventProvider.subEventModel.subEventFee = newSubEventFee;
                      //   subEventProvider.updateSubEventFields(subEventFee: newSubEventFee);
                      // },
                      controller: updateSubEventFeeTextController,
                      hintText: "Sub Event Fee",
                      textInputAction: TextInputAction.next,
                    ),
                    UserInputTextField(
                      // val: subEventProvider.subEventModel.subEventsPersons,
                      // onChangedCallback: (newSubEventPersons){
                      //   // subEventProvider.subEventModel.subEventsPersons = newSubEventPersons;
                      //   subEventProvider.updateSubEventFields(subEventsPersons: newSubEventPersons);
                      // },
                      controller: updateSubEventPersonsTextController,
                      hintText: "Max Persons Allowed",
                      textInputAction: TextInputAction.next,
                    ),
                    UserInputTextField(
                      // val: subEventProvider.subEventModel.subEventDescription,
                      // onChangedCallback: (newSubEventDescription){
                      //   // subEventProvider.subEventModel.subEventDescription = newSubEventDescription;
                      //   subEventProvider.updateSubEventFields(subEventDescription: newSubEventDescription);
                      // },
                      controller: updateSubEventDescriptionTextController,
                      hintText: "Sub Event Description",
                      textInputAction: TextInputAction.done,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.symmetric(horizontal: 60.0),
                            child: MaterialButton(
                              onPressed: (){
                                final subEventModel = SubEventsModel(
                                  subEventID: subEventProvider.subEventModel.subEventID,
                                  mainEventID: subEventProvider.subEventModel.mainEventID,
                                  subEventName: updateSubEventNameTextController.text,
                                  subEventCategory: updateSubEventCategoryTextController.text,
                                  subEventFee: updateSubEventFeeTextController.text,
                                  subEventsPersons: updateSubEventPersonsTextController.text,
                                  subEventDescription: updateSubEventDescriptionTextController.text,
                                );

                                subEventProvider.onUpdateSubEventTapped(
                                    (){
                                      subEventProvider.updateSubEventFields(loading: false);
                                      Navigator.pop(context);
                                      showToast("Updated Successfully");
                                    },
                                    (){
                                      subEventProvider.updateSubEventFields(loading: false);
                                      showToast("Updated Failed");
                                    },
                                    subEventModel
                                );
                              },
                              color: Colors.blue,
                              child: const Text(
                                "Update Sub Event",
                                style: TextStyle(
                                    color: Colors.white
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Visibility(
                      visible: subEventProvider.subEventModel.loading,
                      child: Container(
                        width: 30,
                        height: 30,
                        margin: const EdgeInsets.only(top: 20),
                        child: const CircularProgressIndicator(
                          backgroundColor: Colors.grey,
                        ),
                      ),
                    ),
                  ]
              ),
            ),
          ),
        ),
      );
    });
  }
}
