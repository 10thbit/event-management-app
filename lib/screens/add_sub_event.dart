import 'package:event_manager/models/sub_events_model.dart';
import 'package:event_manager/providers/event_provider.dart';
import 'package:event_manager/providers/sub_event_provider.dart';
import 'package:event_manager/widgets/user_input_textField.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddSubEvent extends StatefulWidget {
  AddSubEvent({Key? key}) : super(key: key);

  @override
  _AddSubEventState createState() => _AddSubEventState();
}

// class _AddSubEventState extends State<AddSubEvent> with ChangeNotifier {
class _AddSubEventState extends State<AddSubEvent> {

  // String? subEventName, subEventCategory, subEventFee, subEventsPersons, subEventDescription;

  final subEventNameTextController = TextEditingController();
  final subEventCategoryTextController = TextEditingController();
  final subEventFeeTextController = TextEditingController();
  final subEventPersonsTextController = TextEditingController();
  final subEventDescriptionTextController = TextEditingController();
  bool shouldPop = true;

  @override
  Widget build(BuildContext context) {
    return Consumer2<EventProvider, SubEventProvider>(builder: (context, eventProvider, subEventProvider, _){
      return WillPopScope(
        onWillPop: () async{
          // eventProvider.eventModel = EventModel();
          // subEventProvider.subEventModel = SubEventsModel();
          return shouldPop;
          },
        child: Scaffold(
          body: SafeArea(
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                          "images/add_event.png",
                          scale: 3
                      ),
                      const SizedBox(height: 20.0,),
                      const Text("Add Sub Event", style: TextStyle(fontSize: 20.0),),
                      const SizedBox(height: 10),
                      Text("Main Event: ${eventProvider.eventModel.eventName}"),
                      const SizedBox(height: 40.0),
                      UserInputTextField(
                        // onChangedCallback: (newSubEventName){
                        //   // subEventName = newSubEventName;
                        //   // subEventProvider.updateSubEventFields(subEventName: newSubEventName);
                        //   subEventProvider.subEventModel.subEventName = newSubEventName;
                        //   // notifyListeners();
                        // },
                        controller: subEventNameTextController,
                        hintText: "Sub Event Name",
                        textInputAction: TextInputAction.next,
                      ),
                      UserInputTextField(
                        // onChangedCallback: (newSubEventCategory){
                        //   // subEventCategory = newSubEventCategory;
                        //   subEventProvider.subEventModel.subEventCategory = newSubEventCategory;
                        //   // subEventProvider.updateSubEventFields(subEventCategory: newSubEventCategory);
                        // },
                        controller: subEventCategoryTextController,
                        hintText: "Sub Event Category",
                        textInputAction: TextInputAction.next,
                      ),
                      UserInputTextField(
                        // onChangedCallback: (newSubEventFee){
                        //   // subEventFee = newSubEventFee;
                        //   subEventProvider.subEventModel.subEventFee = newSubEventFee;
                        //   // subEventProvider.updateSubEventFields(subEventFee: newSubEventFee);
                        // },
                        controller: subEventFeeTextController,
                        hintText: "Sub Event Fee",
                        textInputAction: TextInputAction.next,
                      ),
                      UserInputTextField(
                        // onChangedCallback: (newSubEventPersons){
                        //   // subEventsPersons = newSubEventPersons;
                        //   subEventProvider.subEventModel.subEventsPersons = newSubEventPersons;
                        //   // subEventProvider.updateSubEventFields(subEventsPersons: newSubEventPersons);
                        // },
                        controller: subEventPersonsTextController,
                        hintText: "Max Persons Allowed",
                        textInputAction: TextInputAction.next,
                      ),
                      UserInputTextField(
                        // onChangedCallback: (newSubEventDescription){
                        //   // subEventDescription = newSubEventDescription;
                        //   subEventProvider.subEventModel.subEventDescription = newSubEventDescription;
                        //   // subEventProvider.updateSubEventFields(subEventDescription: newSubEventDescription);
                        // },
                        controller: subEventDescriptionTextController,
                        hintText: "Sub Event Description",
                        textInputAction: TextInputAction.done,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Container(
                              margin: const EdgeInsets.symmetric(horizontal: 60.0),
                              child: MaterialButton(
                                onPressed: (){

                                  var subEventModel = SubEventsModel(
                                    subEventName: subEventNameTextController.text,
                                    subEventCategory: subEventCategoryTextController.text,
                                    subEventFee: subEventFeeTextController.text,
                                    subEventsPersons: subEventPersonsTextController.text,
                                    subEventDescription: subEventDescriptionTextController.text,
                                  );

                                  subEventProvider.onRegisterSubEventTapped(
                                      eventProvider.eventModel.eventID,
                                      (){
                                        subEventProvider.updateSubEventFields(loading: false);
                                        Navigator.pop(context);
                                        print("Added Successfully");
                                      },
                                      (){
                                        subEventProvider.updateSubEventFields(loading: false);
                                        print("Save Failed. Try Again!");
                                      },
                                      subEventModel
                                  );
                                },
                                color: Colors.blue,
                                child: const Text(
                                  "Add Sub Event",
                                  style: TextStyle(
                                      color: Colors.white
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Visibility(
                        visible: subEventProvider.subEventModel.loading,
                        child: Container(
                          width: 30,
                          height: 30,
                          margin: const EdgeInsets.only(top: 20),
                          child: const CircularProgressIndicator(
                            backgroundColor: Colors.grey,
                          ),
                        ),
                      ),
                    ]
                ),
              ),
            ),
          ),
        ),
      );
    });
  }
}
