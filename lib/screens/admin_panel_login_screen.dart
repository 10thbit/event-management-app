import 'package:event_manager/providers/user_provider.dart';
import 'package:event_manager/utilities/routes.dart';
import 'package:event_manager/widgets/user_input_textField.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AdminPanelLogin extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Consumer<UserProvider>( builder: (context, userProvider, child){
      return Scaffold(
        body: Container(
          padding: const EdgeInsets.only(top: 80.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              UserInputTextField(
                onChangedCallback: (admin_email){
                  userProvider.adminEmail = admin_email;
                },
                hintText: "Enter Email",
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.next,
              ),
              UserInputTextField(
                  onChangedCallback: (admin_password)
                  {
                    userProvider.adminPassword = admin_password;
                  },
                  hintText: "Enter Password",
                obscureText: true,
                textInputAction: TextInputAction.done,
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 60.0),
                child: Row(
                  children: [
                    Expanded(
                      child: TextButton(
                        onPressed: (){
                          userProvider.onLoginTapped(context);
                        },
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(Colors.blue),
                        ),
                        child: const Text(
                          "Login",
                          style: TextStyle(
                            color: Colors.white
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: (){
                  Navigator.pushNamed(context, Routes.adminPanelRegisterScreen);
                },
                child: const Text(
                    "Don't have account. Register one here."
                ),
              ),
              Visibility(
                visible: userProvider.userModel.loading,
                child: Container(
                  width: 30,
                  height: 30,
                  margin: const EdgeInsets.only(top: 20),
                  child: const CircularProgressIndicator(
                    backgroundColor: Colors.grey,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    },

    );
  }
}