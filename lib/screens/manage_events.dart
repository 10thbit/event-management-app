import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:event_manager/models/events_model.dart';
import 'package:event_manager/providers/event_provider.dart';
import 'package:event_manager/screens/event_view.dart';
import 'package:event_manager/utilities/routes.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

final _firestore = FirebaseFirestore.instance;
final _auth = FirebaseAuth.instance;

class ManageEvents extends StatefulWidget {

  @override
  _ManageEventsState createState() => _ManageEventsState();
}

class _ManageEventsState extends State<ManageEvents> {
  @override
  Widget build(BuildContext context) {
    return Consumer<EventProvider>(builder: (context, eventProvider, child) {
        return Scaffold(
          body: Container(
            margin: const EdgeInsets.only(top: 80.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Hero(
                      tag:"manage_events",
                      child: Image.asset(
                        "images/manage_event.png",
                        scale: 10,
                      ),
                    ),
                    const SizedBox(width: 5),
                    const Text("Manage Events", style: TextStyle(fontSize: 25),),
                  ],
                ),

                Expanded(
                    child: StreamBuilder<QuerySnapshot>(
                    stream: _firestore.collection('Events').snapshots(),
                    builder: (context, snapshot){
                      if(!snapshot.hasData){
                        return const Center(
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.grey,
                          ),
                        );
                      }
                      List<EventModel> eventsList = snapshot.data!.docs.map((e) => EventModel.fromJson(e)).toList();
                      eventsList.retainWhere((element) => element.creatorID == _auth.currentUser!.uid);
                      return ListView.builder(
                        shrinkWrap: true,
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        itemCount: eventsList.length,
                        itemBuilder: (context, index) {
                          //final item = snapshot.data!.docs[index].data() as Map<String, dynamic>;
                          EventModel eventModel = eventsList[index];

                          return GestureDetector(
                            onTap: (){
                              print(index);
                              eventProvider.eventModel = eventModel;
                              Navigator.pushNamed(context, Routes.eventViewPage);
                              // Get.to(() => EventView());
                              // Get.to(EventView(eventModel: eventModel,));
                              // Navigator.pushNamed(context, 'EventViewPage', arguments: [eventModel]);
                            },
                            child: Container(
                              margin: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 2.0),
                              child: Card(
                                child: Container(
                                  padding: const EdgeInsets.all(10),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(eventModel.eventName, style: const TextStyle(fontSize: 20.0),),
                                      const SizedBox(height: 5.0),
                                      Text("Organizer: ${eventModel.eventOrganizer}", style: const TextStyle(color: Colors.black54),),
                                      const SizedBox(height: 5.0),
                                      Text("Description: ${eventModel.eventDescription}", style: const TextStyle(color: Colors.black54),),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    },
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}