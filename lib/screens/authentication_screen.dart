import 'package:event_manager/providers/user_provider.dart';
import 'package:event_manager/screens/admin_dashboard.dart';
import 'package:event_manager/screens/admin_panel_login_screen.dart';
import 'package:event_manager/screens/user_dashboard.dart';
import 'package:event_manager/utilities/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class AuthenticationScreen extends StatefulWidget {
  const AuthenticationScreen({Key? key}) : super(key: key);

  @override
  State<AuthenticationScreen> createState() => _AuthenticationScreenState();
}

class _AuthenticationScreenState extends State<AuthenticationScreen> {

  @override
  void initState() {
    var provider = Provider.of<UserProvider>(context, listen: false);
    Future.delayed(const Duration(seconds: 2), () {
      viewContent(provider).then((value){
        if(value == true){
          if(provider.userModel.isAdmin == true){
            Navigator.pushNamedAndRemoveUntil(context, Routes.adminPanelDashboard, (route) => false);
          }
          else{
            Navigator.pushNamedAndRemoveUntil(context, Routes.userDashboard, (route) => false);
          }
        }
        else{
          Navigator.pushNamedAndRemoveUntil(context, Routes.adminPanelLoginScreen, (route) => false);
        }
      });
    });
    super.initState();
  }
  Future<bool> viewContent(UserProvider provider) async {
    if (provider.auth.currentUser == null) {
      print('user null');
      return false;
    } else {
      print('user not null');
      await provider.checkUserType();
      return true;
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          // Icon(
          //   Icons.category_rounded,
          //   size: 120,
          //   color: Colors.blue,
          // ),
          Image.asset(
            "images/employee_management.png", scale: 1.2,),
          // SizedBox(height: 20),
          const Center(
            child: Text(
              "the Employee\nManagement",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 30,
                  color: Colors.blue,
                  fontWeight: FontWeight.bold
              ),
            ),
          )
        ],
      ),
    );
  }
}

