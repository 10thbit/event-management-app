import 'package:event_manager/providers/user_provider.dart';
import 'package:event_manager/utilities/constants.dart';
import 'package:event_manager/utilities/routes.dart';
import 'package:event_manager/widgets/toast.dart';
import 'package:event_manager/widgets/user_input_textField.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AdminPanelRegisterScreen extends StatefulWidget {
  @override
  State<AdminPanelRegisterScreen> createState() =>
      _AdminPanelRegisterScreenState();
}

class _AdminPanelRegisterScreenState extends State<AdminPanelRegisterScreen> {
  String? name, email, phone, password, adminType;

  final nameTextController = TextEditingController();
  final emailTextController = TextEditingController();
  final phoneTextController = TextEditingController();
  final passwordTextController = TextEditingController();
  final adminTypeTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Consumer<UserProvider>(
      builder: (context, userProvider, child) {
        return Scaffold(
          body: SafeArea(
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    UserInputTextField(
                      onChangedCallback: (newName) {
                        userProvider.updateUser(userName: newName);
                      },
                      hintText: "Enter your name",
                      textInputAction: TextInputAction.next,
                    ),
                    UserInputTextField(
                      onChangedCallback: (newEmail) {
                        userProvider.updateUser(userEmail: newEmail);
                      },
                      hintText: "Enter your email",
                      keyboardType: TextInputType.emailAddress,
                      textInputAction: TextInputAction.next,
                    ),
                    UserInputTextField(
                      onChangedCallback: (newPhone) {
                        userProvider.updateUser(userPhone: newPhone);
                      },
                      hintText: "Enter your phone",
                      keyboardType: TextInputType.phone,
                      textInputAction: TextInputAction.next,
                    ),
                    const SizedBox(
                      height: 5.0,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                            height: 60.0,
                            color: Colors.grey.shade200,
                            margin:
                                const EdgeInsets.symmetric(horizontal: 60.0),
                            padding:
                                const EdgeInsets.symmetric(horizontal: 10.0),
                            child: Row(
                              children: [
                                const Text(
                                  "Is Admin: ",
                                  style: TextStyle(
                                      color: Colors.black54, fontSize: 16.0),
                                ),
                                const SizedBox(width: 10),
                                Expanded(
                                  child: Row(
                                    children: [
                                      const Text("Yes"),
                                      Radio(
                                        value: true,
                                        groupValue: userProvider.userModel.isAdmin,
                                        activeColor: Colors.lightBlue,
                                        onChanged: (bool? value) {
                                          userProvider.updateUser(isAdmin: value!);
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Row(
                                    children: [
                                      const Text("No"),
                                      Radio(
                                        value: false,
                                        groupValue: userProvider.userModel.isAdmin,
                                        activeColor: Colors.blue,
                                        onChanged: (bool? value) {
                                          userProvider.updateUser(isAdmin: value!);
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Visibility(
                      visible: userProvider.userModel.isAdmin == true,
                      child: Container(
                        margin: const EdgeInsets.only(bottom: 5),
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                height: 60.0,
                                color: Colors.grey.shade200,
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 60.0),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10.0),
                                child: DropdownButton<AdminType>(
                                  value: userProvider.typeFromString(
                                      userProvider.userModel.adminType),
                                  hint: const Text("Choose Admin type"),
                                  alignment: AlignmentDirectional.bottomCenter,
                                  style: const TextStyle(
                                      color: Colors.black54, fontSize: 16.0),
                                  onChanged: (AdminType? newValue) {
                                    userProvider.updateUser(adminType: userProvider.stringAdminType(newValue!));
                                  },
                                  items: AdminType.values
                                      .map((AdminType adminType) {
                                    return DropdownMenuItem<AdminType>(
                                      value: adminType,
                                      child: Text(
                                        userProvider.stringAdminType(adminType),
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    UserInputTextField(
                      onChangedCallback: (newPassword) {
                        userProvider.updateUser(userPassword: newPassword);
                      },
                      hintText: "Enter password",
                      obscureText: true,
                      textInputAction: TextInputAction.done,
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 60.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: TextButton(
                              focusNode: FocusNode(),
                              onPressed: userProvider.userModel.isButtonActive ? () {
                                if (userProvider.userModel.userName.isNotEmpty && userProvider.userModel.userEmail.isNotEmpty && userProvider.userModel.userPhone.isNotEmpty &&
                                    userProvider.userModel.isAdmin.toString().isNotEmpty && userProvider.userModel.userPassword.isNotEmpty) {
                                  print(userProvider.userModel.userName);
                                  print(userProvider.userModel.userEmail);
                                  print(userProvider.userModel.userPhone);
                                  print(userProvider.userModel.isAdmin);
                                  print(userProvider.userModel.adminType);
                                  print(userProvider.userModel.userPassword);

                                  userProvider.updateUser(loading: true, isButtonActive: false);

                                  userProvider.signUpAdmin()
                                      .then((value) async {
                                        userProvider.updateUser(loading: false, isButtonActive: true);
                                    if (value==true) {
                                      bool userType = await userProvider.checkUserType();
                                      if(userType == true){
                                        Navigator.pushNamedAndRemoveUntil(context, Routes.adminPanelDashboard, (route) => false);
                                      }
                                      else{
                                        Navigator.pushNamedAndRemoveUntil(context, Routes.userDashboard, (route) => false);
                                      }
                                      showToast("Registered Successfully");
                                      print("Registered Successfully");
                                    }
                                    else {
                                      showToast("Something went wrong");
                                      print("Something gone bad!");
                                    }
                                  });
                                }
                                else{
                                  showToast("Fields Required");
                                }
                              } : null,
                              style: userProvider.userModel.isButtonActive ? ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                    Colors.blue),
                              ) : ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                    Colors.lightBlueAccent.shade100),
                              ) ,
                              child: const Text(
                                "Register",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: const Text(
                          "Already Registered? Login to your account."),
                    ),
                    Visibility(
                      visible: userProvider.userModel.loading,
                      child: Container(
                        width: 30,
                        height: 30,
                        margin: const EdgeInsets.only(top: 20),
                        child: const CircularProgressIndicator(
                          backgroundColor: Colors.grey,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
